## Custom Grid Overview
The following is a screenshot of what the finished custom grid looks like in the document.

![alt text](https://gitlab.com/andrelai/CustomGrid/-/raw/master/CustomGridOverview.png)
## Table Setup
Set up table with a unique primary key. Do NOT use the DocumentVersionId as the primary key. A foreign key reference and constraint should be set up for the DocumentVersionId. (see Database > DataModel > create-CustomGridPhoneList.sql)

## Document Management (DocumentCodes table)
The table list (DocumentCode.TableList) must contain your grid table

## Get Stored procedure
In the main get stored procedure (see Database > StoredProcedure > csp_GetCustomDocumentTest.sql), add a select statement from your grid table. If you want to text to show for the global code drop down instead of the global code id, return the text in a seperate column (see column `RelationshipText`).
````sql
-- query for custom grid
SELECT cga.CustomGridPhoneListId
	  ,cga.DocumentVersionId
	  ,cga.CreatedBy
	  ,cga.CreatedDate
	  ,cga.ModifiedBy
	  ,cga.ModifiedDate
	  ,cga.RecordDeleted
	  ,cga.DeletedBy
	  ,cga.DeletedDate
	  ,cga.NameOfPerson
	  ,cga.PhoneNumber
	  ,cga.Relationship
	  ,RelationshipText = dbo.csf_GetGlobalCodeNameById(cga.Relationship)
    ,cga.StartDate
FROM CustomGridPhoneList AS cga
where DocumentVersionId = @DocumentVersionId
	and isnull(RecordDeleted, 'N') = 'N
````
## Dataset Deployment
1. Add all the tables to your dataset including your table for the custom grid.
1. Prior to deploying the dataset. Set the ReadOnly property for the Primary Key value to "false"
![alt text](https://gitlab.com/andrelai/CustomGrid/-/raw/master/PrimaryKeyDataSet.png)
1. If you are using a global code column and want the text to appear in the grid instead of the global code, add a column of System.String type
![alt text](https://gitlab.com/andrelai/CustomGrid/-/raw/master/GlobalCodeColumn.png)

## Control page (.ascx)


Use below code to create the custom grid. There are 3 sections in the grid. 
1. The fields that populate the grid
2. The buttons that perform the action of insert into the grid, modifying the grid, or clearing the fields.
3. The grid that shows the inserted data.

A few things to note when working with your controls
1. in the HiddenFieldPageTables input, **DO NOT** include the grid table in the list. (see WebPages > Main.ascx)
1. Add the following register using the below line of code in your control page (.ascx): `<%@ Register Src="~/CommonUserControls/CustomGrid.ascx" TagName="CustomGrid" TagPrefix="uc1" %>` (see WebPages > General.ascx)
1. create input fields for columns that need editing. (see WebPages > General.ascx)
1. add parentchildcontrols="True" attribute to the fields. (see WebPages > General.ascx)
1. add insert button, which is used to perform insert/modify operation. (see WebPages > General.ascx)
1. add clear button to clear the fields when necessary. (see WebPages > General.ascx)
1. add the insert button Id to the CustomGrid attribute "InsertButtonId" (see WebPages > General.ascx)
1. add hiddenfields for PrimaryKey and Foreign key columns wih parentchildcontrols attribute set to True (see WebPages > General.ascx)

````html
<table cellspacing="0" cellpadding="0" border="0" width="99%" style="margin-left: 2px;" id="TableChildControl_CustomGridPhoneList">
  <tr>
    <td class="checkbox_container">
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
          <td width="7">
            <img style="vertical-align: top;" height="26" width="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_left1.gif" />
          </td>
          <td class="content_tab_top" width="100%" />
          <td width="7">
            <img style="vertical-align: top;" height="26" width="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_right.gif" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" class="content_tab_bg" align="left">
      <table border="0" cellspacing="0" cellpadding="0" width="100%" >
        <tr>
          <td style="padding-left: 10px;">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
              <!-- fields -->
              <tr>
                <td><strong>Phone list</strong></td>
              </tr>
              <tr>
                <td>
                  <span class="required">*&nbsp;</span> Name of Person:
                </td>
                <td>
                  <input style="width: 200px" class="form_textbox element"
                    id="TextBox_CustomGridPhoneList_NameOfPerson"
                    name="TextBox_CustomGridPhoneList_NameOfPerson"
                    parentchildcontrols="True">
                </td>
              </tr>
              <tr>
                <td>Phone Number:
                </td>
                <td>
                  <input style="width: 200px" class="form_textbox element"
                    id="TextBox_CustomGridPhoneList_PhoneNumber"
                    name="TextBox_CustomGridPhoneList_PhoneNumber"
                    datatype="PhoneNumber" onchange="PhoneFormatCheck(this)" parentchildcontrols="True">
                </td>
              </tr>
              <tr>
                <td>Relationship
                </td>
                <td>
                  <cc2:DropDownGlobalCodes
                    ID="DropDownList_CustomGridPhoneList_Relationship"
                    Width="200px" AppendDataBoundItems="true" runat="server" Category="RELATIONSHIP"
                    CssClass="form_dropdown"
                    parentchildcontrols="True">
                  </cc2:DropDownGlobalCodes>
                </td>
              </tr>
              <tr>
                <td>Start Date</td>
                <td>
                  <input class="form_textbox" type="text" id="TextBox_CustomGridPhoneList_StartDate" datatype="Date" name="TextBox_CustomGridPhoneList_StartDate"/>
                  <img id="img1" src="<%=RelativePath%>App_Themes/Includes/Images/calender_grey.gif"
                  onclick="return showCalendar('TextBox_CustomGridPhoneList_StartDate', '%m/%d/%Y');" style="cursor: hand; vertical-align: text-bottom;"
                  parentchildcontrols="True"/>
                </td>
              </tr>
              <!-- buttons -->
              <tr>
                <td>&nbsp;</td>
                <td align="left" style="padding: 5px 0px 5px 0px">
                  <!-- insert button -->
                  <input class="parentchildbutton" type="button" value="Insert" id="Button_CustomGridPhoneList_ButtonInsert"
                    name="Button_CustomGridPhoneList_ButtonInsert" baseurl="<%=ResolveUrl("~") %>"
                    onclick="return InsertCustomGrid('TableChildControl_CustomGridPhoneList', 'DivCustomGridPhoneList', 'CustomGridCustomGridPhoneList', this);" />
                  <!-- clear button -->
                  <input class="parentchildbutton" type="button" value="Clear" id="Button_CustomGridPhoneList_Clear"
                    name="Button_CustomGridPhoneList_Clear" baseurl="<%=ResolveUrl("~") %>" style="margin-right: 23px; padding-left: 10px"
                    onclick="ClearCustomGrid('TableChildControl_CustomGridPhoneList', 'Button_CustomGridPhoneList_ButtonInsert');" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="height2"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="checkbox_container">
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
          <td class="right_bottom_cont_bottom_bg" width="2">
            <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_left.gif" />
          </td>
          <td class="right_bottom_cont_bottom_bg" width="100%"></td>
          <td class="right_bottom_cont_bottom_bg" align="right" width="2">
            <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_right.gif" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="height2">&nbsp;</td>
  </tr>
  <!-- grid -->
  <tr>
    <td style="width: 86%">
      <div id="DivCustomGridPhoneList" runat="server">
        <uc1:CustomGrid runat="server"
          ID="CustomGridCustomGridPhoneList"
          TableName="CustomGridPhoneList"
          PrimaryKey="CustomGridPhoneListId"
          foreignkey="DocumentVersionId"
          CustomGridTableName="TableChildControl_CustomGridPhoneList"
          GridPageName="CustomGridPhoneList"
          ColumnName="NameOfPerson:PhoneNumber:RelationshipText:StartDate"
          ColumnHeader="Name of Person:Phone Number:Relationship:Start Date"
          ColumnWidth="25%:25%:25%:25%"
          DivGridName="DivCustomGridPhoneList"
          DoNotDisplayDeleteImage="False"
          DoNotDisplayRadio="False"
          ColumnFormat=":::Date"
          InsertButtonId="Button_CustomGridPhoneList_ButtonInsert" />
      </div>

      <input type="hidden" id="HiddenField_CustomGridPhoneList_CustomGridPhoneListId" name="HiddenField_CustomGridPhoneList_CustomGridPhoneListId" parentchildcontrols="True" value="-1" />
      <input type="hidden" id="HiddenField_CustomGridPhoneList_DocumentVersionId" name="HiddenField_CustomGridPhoneList_DocumentVersionId" value="-1" parentchildcontrols="True" />
      <input type="hidden" id="HiddenFieldPrimaryKey" name="HiddenFieldPrimaryKey" value="CustomGridPhoneListId" />
      <input type="hidden" id="HiddenFieldForigenKey" name="HiddenFieldForigenKey" value="DocumentVersionId" />
    </td>
  </tr>
</table>
````
## Code behind (.ascx.cs)
1. In your main c# page, **DO NOT** include the grid in the TablesToBeInitialized function (see WebPages > Main.ascx.cs)

1. Add the following bind in the code behind of the control you are adding the grid to (see WebPages > General.ascx.cs): `CustomGridCustomGridPhoneList.Bind(ParentDetailPageObject.ScreenId);`

## javascript
### Parent Child Custom Grid Insert
The following function is used to insert the grid data. It checks for a required field and returns a false that will cause an error message to show and prevent the insert. 
````javascript
function InsertCustomGrid(gridDatatable, gridDivName, dataGridID, buttonCtrl)
{
  if ($("#TextBox_CustomGridPhoneList_NameOfPerson").val() == '')
  {
    ShowHideErrorMessage("'Name of Person' is required.", "true", "true", "Alert");
    return false;
  }
  else {
    InsertGridData(gridDatatable, gridDivName, dataGridID, buttonCtrl);
  }
}
````

### Parent Child Custom Grid Clear
The following function is used to clear the data.
````javascript
function ClearCustomGrid(gridDatatable, buttonCtrl) {
  ClearTable(gridDatatable, buttonCtrl, false);
}
````