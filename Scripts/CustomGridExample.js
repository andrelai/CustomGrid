function InsertCustomGrid(gridDatatable, gridDivName, dataGridID, buttonCtrl)
{
  if ($("#TextBox_CustomGridPhoneList_NameOfPerson").val() == '')
  {
    ShowHideErrorMessage("'Name of Person' is required.", "true", "true", "Alert");
    return false;
  }
  else {
    InsertGridData(gridDatatable, gridDivName, dataGridID, buttonCtrl);
  }
}

function ClearCustomGrid(gridDatatable, buttonCtrl) {
  ClearTable(gridDatatable, buttonCtrl, false);
}