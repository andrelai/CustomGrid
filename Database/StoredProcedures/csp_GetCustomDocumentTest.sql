if OBJECT_ID('csp_GetCustomDocumentTest', 'P') is not null
	drop procedure csp_GetCustomDocumentTest
go

create procedure csp_GetCustomDocumentTest
	@DocumentVersionId int
as

/******************************************************************************************  
* Stored Procedure: csp_GetCustomDocumentTest            
*       Date			Author                  Purpose          
-------------------------------------------------------------------------------------------  
*	2020-11-18			alai/bbt		 			
******************************************************************************************/

-- query for main data table
select cdmt.DocumentVersionId
	  ,cdmt.CreatedBy
	  ,cdmt.CreatedDate
	  ,cdmt.ModifiedBy
	  ,cdmt.ModifiedDate
	  ,cdmt.RecordDeleted
from CustomDocumentMainTable as cdmt
where DocumentVersionId = @DocumentVersionId
  and isnull(RecordDeleted, 'N') = 'N'

-- query for custom grid
SELECT cga.CustomGridPhoneListId
	  ,cga.DocumentVersionId
	  ,cga.CreatedBy
	  ,cga.CreatedDate
	  ,cga.ModifiedBy
	  ,cga.ModifiedDate
	  ,cga.RecordDeleted
	  ,cga.DeletedBy
	  ,cga.DeletedDate
	  ,cga.NameOfPerson
	  ,cga.PhoneNumber
	  ,cga.Relationship
	  ,RelationshipText = dbo.csf_GetGlobalCodeNameById(cga.Relationship)
    ,cga.StartDate
FROM CustomGridPhoneList AS cga
where DocumentVersionId = @DocumentVersionId
	and isnull(RecordDeleted, 'N') = 'N'

  --Checking For Errors           
If (@@error!=0)                
Begin                           
RAISERROR ('[csp_GetCustomDocumentTest] : An Error Occured',16,1)               
Return                         
End
go