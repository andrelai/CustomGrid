IF OBJECT_ID('dbo.CustomGridPhoneList', 'U') IS NOT NULL
	DROP TABLE dbo.CustomGridPhoneList
GO

CREATE TABLE CustomGridPhoneList (
	[CustomGridPhoneListId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[DocumentVersionId] INT NOT NULL,
	[CreatedBy] [dbo].[type_CurrentUser] NOT NULL,
	[CreatedDate] [dbo].[type_CurrentDatetime] NOT NULL,
	[ModifiedBy] [dbo].[type_CurrentUser] NOT NULL,
	[ModifiedDate] [dbo].[type_CurrentDatetime] NOT NULL,
	[RecordDeleted] [dbo].[type_YOrN] NULL,
	[DeletedBy] [dbo].[type_UserId] NULL,
	[DeletedDate] [datetime] NULL,
	[NameOfPerson] [dbo].type_Comment2 NULL,
	[PhoneNumber] [dbo].type_PhoneNumber NULL,
	[Relationship] [dbo].type_GlobalCode NULL,
  [StartDate] datetime null,
)
GO

ALTER TABLE [dbo].[CustomGridPhoneList]  WITH CHECK ADD  CONSTRAINT [DocumentVersions_CustomGridPhoneList_FK] FOREIGN KEY([DocumentVersionId])
REFERENCES [dbo].[DocumentVersions] ([DocumentVersionId])
GO

ALTER TABLE [dbo].[CustomGridPhoneList] CHECK CONSTRAINT [DocumentVersions_CustomGridPhoneList_FK]
GO