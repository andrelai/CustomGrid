﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="General.ascx.cs" Inherits="SHS.SmartCare.Custom_CustomDocumentAndre_WebPages_General" %>
<%@ Register Assembly="Streamline.DotNetDropDownGlobalCodes" Namespace="Streamline.DotNetDropDownGlobalCodes" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUserControls/CustomGrid.ascx" TagName="CustomGrid" TagPrefix="uc1" %>
<div class="DocumentScreen">
  <table cellspacing="0" cellpadding="0" border="0" width="100%">

    <!-- My First Section -->
    <!-- header -->
    <tr>
      <td>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td class="content_tab_left" align="left" nowrap='nowrap'>My First Section</td>
            <td width="17">
              <img style="vertical-align: top;" height="26" width="17" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_sep.gif" />
            </td>
            <td class="content_tab_top" width="100%" />
            <td width="7">
              <img style="vertical-align: top;" height="26" width="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_right.gif" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- content -->
    <tr>
      <td valign="top" class="content_tab_bg" align="center">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td style="padding: 0px 0px 0px 10px;">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="0" border="0" width="99%" style="margin-left: 2px;" id="TableChildControl_CustomGridPhoneList">
                      <tr>
                        <td class="checkbox_container">
                          <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                              <td width="7">
                                <img style="vertical-align: top;" height="26" width="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_left1.gif" />
                              </td>
                              <td class="content_tab_top" width="100%" />
                              <td width="7">
                                <img style="vertical-align: top;" height="26" width="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/content_tab_right.gif" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" class="content_tab_bg" align="left">
                          <table border="0" cellspacing="0" cellpadding="0" width="100%" >
                            <tr>
                              <td style="padding-left: 10px;">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                  <!-- fields -->
                                  <tr>
                                    <td><strong>Phone list</strong></td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <span class="required">*&nbsp;</span> Name of Person:
                                    </td>
                                    <td>
                                      <input style="width: 200px" class="form_textbox element"
                                        id="TextBox_CustomGridPhoneList_NameOfPerson"
                                        name="TextBox_CustomGridPhoneList_NameOfPerson"
                                        parentchildcontrols="True">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Phone Number:
                                    </td>
                                    <td>
                                      <input style="width: 200px" class="form_textbox element"
                                        id="TextBox_CustomGridPhoneList_PhoneNumber"
                                        name="TextBox_CustomGridPhoneList_PhoneNumber"
                                        datatype="PhoneNumber" onchange="PhoneFormatCheck(this)" parentchildcontrols="True">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Relationship
                                    </td>
                                    <td>
                                      <cc2:DropDownGlobalCodes
                                        ID="DropDownList_CustomGridPhoneList_Relationship"
                                        Width="200px" AppendDataBoundItems="true" runat="server" Category="RELATIONSHIP"
                                        CssClass="form_dropdown"
                                        parentchildcontrols="True">
                                      </cc2:DropDownGlobalCodes>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Start Date</td>
                                    <td>
                                      <input class="form_textbox" type="text" id="TextBox_CustomGridPhoneList_StartDate" datatype="Date" name="TextBox_CustomGridPhoneList_StartDate"/>
                                      <img id="img1" src="<%=RelativePath%>App_Themes/Includes/Images/calender_grey.gif"
                                      onclick="return showCalendar('TextBox_CustomGridPhoneList_StartDate', '%m/%d/%Y');" style="cursor: hand; vertical-align: text-bottom;"
                                      parentchildcontrols="True"/>
                                    </td>
                                  </tr>
                                  <!-- buttons -->
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td align="left" style="padding: 5px 0px 5px 0px">
                                      <!-- insert button -->
                                      <input class="parentchildbutton" type="button" value="Insert" id="Button_CustomGridPhoneList_ButtonInsert"
                                        name="Button_CustomGridPhoneList_ButtonInsert" baseurl="<%=ResolveUrl("~") %>"
                                        onclick="return InsertCustomGrid('TableChildControl_CustomGridPhoneList', 'DivCustomGridPhoneList', 'CustomGridCustomGridPhoneList', this);" />
                                      <!-- clear button -->
                                      <input class="parentchildbutton" type="button" value="Clear" id="Button_CustomGridPhoneList_Clear"
                                        name="Button_CustomGridPhoneList_Clear" baseurl="<%=ResolveUrl("~") %>" style="margin-right: 23px; padding-left: 10px"
                                        onclick="ClearCustomGrid('TableChildControl_CustomGridPhoneList', 'Button_CustomGridPhoneList_ButtonInsert');" />
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td class="height2"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="checkbox_container">
                          <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                              <td class="right_bottom_cont_bottom_bg" width="2">
                                <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_left.gif" />
                              </td>
                              <td class="right_bottom_cont_bottom_bg" width="100%"></td>
                              <td class="right_bottom_cont_bottom_bg" align="right" width="2">
                                <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_right.gif" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="height2">&nbsp;</td>
                      </tr>
                      <!-- grid -->
                      <tr>
                        <td style="width: 86%">
                          <div id="DivCustomGridPhoneList" runat="server">
                            <uc1:CustomGrid runat="server"
                              ID="CustomGridCustomGridPhoneList"
                              TableName="CustomGridPhoneList"
                              PrimaryKey="CustomGridPhoneListId"
                              foreignkey="DocumentVersionId"
                              CustomGridTableName="TableChildControl_CustomGridPhoneList"
                              GridPageName="CustomGridPhoneList"
                              ColumnName="NameOfPerson:PhoneNumber:RelationshipText:StartDate"
                              ColumnHeader="Name of Person:Phone Number:Relationship:Start Date"
                              ColumnWidth="25%:25%:25%:25%"
                              DivGridName="DivCustomGridPhoneList"
                              DoNotDisplayDeleteImage="False"
                              DoNotDisplayRadio="False"
                              ColumnFormat=":::Date"
                              InsertButtonId="Button_CustomGridPhoneList_ButtonInsert" />
                          </div>

                          <input type="hidden" id="HiddenField_CustomGridPhoneList_CustomGridPhoneListId" name="HiddenField_CustomGridPhoneList_CustomGridPhoneListId" parentchildcontrols="True" value="-1" />
                          <input type="hidden" id="HiddenField_CustomGridPhoneList_DocumentVersionId" name="HiddenField_CustomGridPhoneList_DocumentVersionId" value="-1" parentchildcontrols="True" />
                          <input type="hidden" id="HiddenFieldPrimaryKey" name="HiddenFieldPrimaryKey" value="CustomGridPhoneListId" />
                          <input type="hidden" id="HiddenFieldForigenKey" name="HiddenFieldForigenKey" value="DocumentVersionId" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
        </table>
      </td>
    </tr>
    <!-- footer -->
    <tr>
      <td colspan="5">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td class="right_bottom_cont_bottom_bg" width="2">
              <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_left.gif" />
            </td>
            <td class="right_bottom_cont_bottom_bg" width="100%"></td>
            <td class="right_bottom_cont_bottom_bg" align="right" width="2">
              <img style="vertical-align: top;" height="7" alt="" src="<%=RelativePath%>App_Themes/Includes/images/right_bottom_cont_bottom_right.gif" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="height2">&nbsp;</td>
    </tr>
  </table>
</div>

