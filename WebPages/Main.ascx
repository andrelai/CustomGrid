﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Main.ascx.cs" Inherits="SHS.SmartCare.Custom_CustomDocumentAndre_WebPages_Main" %>
<% if (HttpContext.Current == null) { %>
<link href="../../../App_Themes/Styles/smartcare_styles.css" rel="stylesheet" type="text/css"/>
<%} %>

<script type="text/javascript" language="javascript" src="<%=RelativePath%>JScripts/ApplicationScripts/GeneralFormFunctions.js"></script>
<script type="text/javascript" language="javascript" src="<%=RelativePath%>JScripts/ApplicationScripts/GeneralArrayHelpers.js"></script>

<script type="text/javascript" src="<%=RelativePath%>Custom/CustomDocumentAndre/Scripts/Andre.js"></script>

<asp:Panel ID="PanelMain" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="margin-left: 0; margin-right: 0">
                <dxtc:ASPxPageControl ID="AndreTabPageInstance" ClientInstanceName="AndreTabPageInstance"
                  runat="server" ActiveTabIndex="4" CssFilePath="~/App_Themes/Styles/DevExpressTabStyles.css"
                  Width="100%" Paddings-Padding="0px" ContentStyle-Paddings-PaddingRight="0px"
                  ContentStyle-Paddings-PaddingLeft="0px" ContentStyle-BorderRight-BorderWidth="0"
                  ContentStyle-BorderLeft-BorderWidth="0px">
                    <ClientSideEvents TabClick="function(s, e) {        
        onIndividualTabSelected(s,e);      
       }" ActiveTabChanged="function(s, e) {onActiveTabChanged(s, e)}" />
                    <TabPages>
                        <dxtc:TabPage Text="General" Name="/Custom/CustomDocumentAndre/WebPages/General.ascx">
                            <ContentCollection>
                                <dxw:ContentControl ID="General" runat="server">
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                    </TabPages>
                </dxtc:ASPxPageControl>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="PanelCustomAjax" runat="server">
</asp:Panel>

<input id="HiddenFieldPageTables" name="HiddenFieldPageTables" type="hidden" value="CustomDocumentAndre"/>  <!-- do not put grid tables in here -->

