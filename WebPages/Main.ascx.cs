﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SHS.SmartCare
{
  public partial class Custom_CustomDocumentAndre_WebPages_Main : SHS.BaseLayer.ActivityPages.DocumentDataActivityMultiTabPage
  {
    public override string DefaultTab
    {
      get { return "/Custom/CustomDocumentAndre/WebPages/General.ascx"; }
    }

    public override string MultiTabControlName
    {
      get { return AndreTabPageInstance.ID; }
    }

    public override string PageDataSetName
    {
      get { return "DataSetCustomAndre"; }
    }

    public override string[] TablesToBeInitialized
    {
      get { return new string[] { "CustomDocumentAndre" }; } //don't add grid tables
    }

    public override void BindControls()
    {
      
    }

    public override void setTabIndex(int TabIndex, out ControlCollection ctlcollection, out string UcPath)
    {
      AndreTabPageInstance.ActiveTabIndex = (short)TabIndex;
      ctlcollection = AndreTabPageInstance.TabPages[TabIndex].Controls;
      UcPath = AndreTabPageInstance.TabPages[TabIndex].Name;
    }

  }

}